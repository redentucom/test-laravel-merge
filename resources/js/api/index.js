import axios from 'axios'

/**
 * Get all combinations of given sets
 *
 * @param data
 * @return {*}
 */
export const getAllCombinationsRequest = (data) => {
  return axios.post('/api/merge', data)
}

export const getCombinationCountRequest = (sets) => {
  return axios.post('/api/merge/count', {sets: sets})
}
