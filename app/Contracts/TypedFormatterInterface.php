<?php

namespace App\Contracts;

interface TypedFormatterInterface
{
	public static function operator() : string;
}
