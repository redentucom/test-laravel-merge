<?php

namespace App\Contracts;

interface LineFormatterInterface
{
	/**
	 * Apply line format
	 *
	 * @return string
	 */
	public function apply() : string;
}
