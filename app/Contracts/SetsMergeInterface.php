<?php

namespace App\Contracts;

interface SetsMergeInterface
{
	/**
	 * Should merge items to vector list
	 *
	 * @param $separator
	 * @param $wrapper
	 * @param $case
	 *
	 * @return array
	 */
	public function merge($separator, $wrapper, $case) : array;

	/**
	 * Get number of all possible combinations
	 *
	 * @return int
	 */
	public function getCount() : int;
}
