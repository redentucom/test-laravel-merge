<?php

namespace App\Services;

use App\Contracts\LineFormatterInterface;
use App\Contracts\SetsMergeInterface;
use App\LineFormatter;
use App\Services\Decorators\DummyDecorator;
use App\Services\Decorators\LineCase\CamelCaseDecorator;
use App\Services\Decorators\LineCase\DashCaseDecorator;
use App\Services\Decorators\LineCase\LowerCaseDecorator;
use App\Services\Decorators\LineCase\NormalCaseDecorator;
use App\Services\Decorators\LineCase\SnakeCaseDecorator;
use App\Services\Decorators\LineCase\StudlyCaseDecorator;
use App\Services\Decorators\LineCase\TitleCaseDecorator;
use App\Services\Decorators\LineCase\UpperCaseDecorator;
use App\Services\Decorators\LineJoin\LineJoinDecorator;
use App\Services\Decorators\LineWrapp\LineWrapDecorator;

class WordSetsMergeService implements SetsMergeInterface
{
	/**
	 * Raw sets
	 *
	 * @var array
	 */
	protected $sets;

	/**
	 * @var array
	 */
	protected static $caseDecoratos = [
		CamelCaseDecorator::class,
		DashCaseDecorator::class,
		LowerCaseDecorator::class,
		NormalCaseDecorator::class,
		SnakeCaseDecorator::class,
		StudlyCaseDecorator::class,
		TitleCaseDecorator::class,
		UpperCaseDecorator::class,
	];

	public function __construct(array $sets)
	{
		$this->sets = $sets;
	}

	/**
	 * Get number of all possible combinations
	 *
	 * @return int
	 */
	public function getCount(): int
	{
		return collect($this->sets)->reduce(function ($carry, array $set) {
			if (empty($set)) {
				return $carry;
			}

			return $carry * count($set);
		}, 1);
	}

	/**
	 * Should merge items to vector list
	 *
	 * First step is to apply case for each of words set
	 * Second step is n line of each words set and join it with character
	 * Third step is to wrap resulting line by characters
	 *
	 * @param $separator
	 * @param $wrapper
	 * @param $case
	 * @return array
	 */
	public function merge($separator, $wrapper, $case): array
	{
		$this->applyCase($case);
		$vector = $this->mix($separator, $wrapper);

		return $vector;
	}

	/**
	 * Return merged lines with glue as separator
	 *
	 * @param $separator
	 * @return array
	 */
	protected function mix($separator, $wrapper) : array
	{
		$combinations = $this->cartesianMix($this->sets, $separator, $wrapper);

		$result = [];

		foreach ($combinations as $combination) {
			/*
			 * Since decorator accepts previous decorator as first argument
			 * I made a trick that the rest of decorators comes as second
			 * argument of the array, to later be joined with separator
			 */
			$line = new LineJoinDecorator($combination[0], array_slice($combination, 1), $separator);
			$line = new LineWrapDecorator($line, $wrapper);

			array_push(
				$result,
				$line->apply()
			);
		}

		return $result;
	}

	/**
	 * Make all possible combination using cartesian product algorithm
	 *
	 * @param array $input
	 * @param string $separator
	 * @param string $wrapper
	 * @return array
	 */
	protected function cartesianMix(array $input, string $separator, string $wrapper) : array
	{
		if (empty($input)) {
			return [[]];
		}

		$item = array_shift($input);

		// In case current iterable item is an empty array, skip it
		if (empty($item)) {
			return [[]];
		}

		$mix = $this->cartesianMix($input, $separator, $wrapper);

		$result = [];

		foreach ($item as $itemValue) {
			foreach ($mix as $mixValue) {
				array_push(
					$result,
					array_merge([new LineFormatter($itemValue)], $mixValue)
				);
			}
		}

		return $result;
	}

	/**
	 * Apply case format to the each set
	 *
	 * @param $format
	 * @return SetsMergeInterface
	 */
	protected function applyCase($format) : SetsMergeInterface
	{
		$formatter = $this->resolveCaseDecoratorClass($format);

		for ($i = 0; $i < count($this->sets); $i++) {
			for ($j = 0; $j < count($this->sets[$i]); $j++) {
				/** @var LineFormatterInterface $line */
				$line = new $formatter(new LineFormatter($this->sets[$i][$j]));
				$this->sets[$i][$j] = $line->apply();
			}
		}

		return $this;
	}

	/**
	 * Return full namespace for case decorator
	 *
	 * @param $operator
	 * @return string
	 */
	protected function resolveCaseDecoratorClass($operator) : string
	{
		$decorator = collect(static::$caseDecoratos)->first(function ($namespace) use ($operator) {
			if (method_exists($namespace, 'operator')) {
				return forward_static_call([$namespace, 'operator']) === $operator;
			}

			return false;
		});

		return $decorator ?? DummyDecorator::class;
	}
}
