<?php

namespace App\Services\Decorators;

use App\Contracts\LineFormatterInterface;

/**
 * Class LineFormatterDecorator
 *
 * Helper class for easier decorator implementation
 */
abstract class LineFormatterDecorator implements LineFormatterInterface
{
	/**
	 * @var LineFormatterInterface
	 */
	protected $formatter;

	public function __construct(LineFormatterInterface $formatter)
	{
		$this->formatter = $formatter;
	}

	public function apply(): string
	{
		return $this->formatter->apply();
	}
}
