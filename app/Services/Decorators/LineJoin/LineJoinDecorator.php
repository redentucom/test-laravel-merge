<?php

namespace App\Services\Decorators\LineJoin;

use App\Contracts\LineFormatterInterface;
use App\Services\Decorators\LineFormatterDecorator;
use Illuminate\Support\Arr;

class LineJoinDecorator extends LineFormatterDecorator
{
	/**
	 * @var LineFormatterInterface[]
	 */
	protected $sets;

	/***
	 * @var string
	 */
	protected $glue;

	/**
	 * LineJoinDecorator constructor.
	 * @param LineFormatterInterface $formatter
	 * @param array $sets
	 * @param string $glue
	 */
	public function __construct(LineFormatterInterface $formatter, array $sets, string $glue)
	{
		parent::__construct($formatter);

		$this->sets = $sets;
		$this->glue = $glue;
	}

	/**
	 * Join different sets using glue string
	 *
	 * @return string
	 */
	public function apply(): string
	{
		return implode(
			$this->glue,
			$this->getSetsValues()
		);
	}

	protected function getSetsValues() : array
	{
		/**
		 * Make one dim array of each word/line/decorator
		 */
		$items = Arr::flatten([$this->formatter, $this->sets]);

		return array_map(function (LineFormatterInterface $formatter) {
			return $formatter->apply();
		}, $items);
	}
}
