<?php

namespace App\Services\Decorators;

/**
 * Class DummyDecorator
 *
 * @author Andriy Butnar <andriy.butnar@redentu.com>
 */
class DummyDecorator extends LineFormatterDecorator {}
