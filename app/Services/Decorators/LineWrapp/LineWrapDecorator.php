<?php

namespace App\Services\Decorators\LineWrapp;

use App\Contracts\LineFormatterInterface;
use App\Services\Decorators\LineFormatterDecorator;

class LineWrapDecorator extends LineFormatterDecorator
{
	/**
	 * @var
	 */
	protected $wrapper;

	public function __construct(LineFormatterInterface $formatter, string $wrapper)
	{
		parent::__construct($formatter);

		$this->wrapper = $wrapper;
	}

	/**
	 * Wrap line
	 *
	 * @return string
	 */
	public function apply(): string
	{
		[$left, $right] = $this->getWrappers();

		return $left . parent::apply() . $right;
	}

	/**
	 * Return left and right wrappers
	 *
	 * @return array
	 */
	protected function getWrappers() : array
	{
		if (strlen($this->wrapper) >= 2) {
			return str_split($this->wrapper);
		}

		return [$this->wrapper, $this->wrapper];
	}
}
