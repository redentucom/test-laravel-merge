<?php

namespace App\Services\Decorators\LineCase;

use App\Contracts\TypedFormatterInterface;
use App\Services\Decorators\LineFormatterDecorator;
use Illuminate\Support\Str;

class TitleCaseDecorator extends LineFormatterDecorator implements TypedFormatterInterface
{
	/**
	 * Operator value
	 *
	 * @return string
	 */
	public static function operator(): string
	{
		return 'title_case';
	}

	/**
	 * @return string
	 */
	public function apply(): string
	{
		return Str::title(parent::apply());
	}
}
