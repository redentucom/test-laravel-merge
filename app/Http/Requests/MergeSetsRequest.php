<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MergeSetsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sets' => 'array',
	        'sets.*' => 'array',
	        'sets.*.*' => 'string',
	        'separator' => 'max:1000',
	        'wrapper' => 'nullable|string',
	        'case' => 'required|string|in:snake_case,dash_case,camel_case,studly_case,upper_case,lower_case,title_case,normal_case,none',
        ];
    }
}
