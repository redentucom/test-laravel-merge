<?php

namespace App\Http\Controllers;

use App\Http\Requests\CalculateMergeCombinationsRequest;
use App\Http\Requests\MergeSetsRequest;
use App\Services\WordSetsMergeService;
use Illuminate\Http\Request;

class MergeController extends Controller
{
	/**
	 * Merge set of words into one dimensional array
	 *
	 * @param MergeSetsRequest $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function merge(MergeSetsRequest $request)
	{
		$set = new WordSetsMergeService($request->sets ?? []);

		$result = $set->merge(
			$request->separator ?? ' ',
			$request->wrapper,
			$request->case
		);


		return response()->json(compact('result'));
	}

	/**
	 * Calculate number of all possible combinations
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function calculateMergesCount(CalculateMergeCombinationsRequest $request)
	{
		$set = new WordSetsMergeService($request->sets ?? []);

		return response()->json([
			'count' => $set->getCount()
		]);
	}
}
