<?php

namespace App;

use App\Contracts\LineFormatterInterface;

class LineFormatter implements LineFormatterInterface
{
	/**
	 * @var string
	 */
	protected $line;

	public function __construct(string $line)
	{
		$this->line = $line;
	}

	/**
	 * Apply line format
	 *
	 * @return string
	 */
	public function apply(): string
	{
		return $this->line;
	}
}
